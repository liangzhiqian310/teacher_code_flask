"""这是实现短信验证发送功能的第三方

    容联云

    具体的接口实现

    1.必须拥有容联云账号

    2.然后替换相关的配置信息

        # 说明：主账号，登陆云通讯网站后，可在"控制台-应用"中看到开发者主账号ACCOUNT SID
        _accountSid = '8aaf070863f8fb0401641cb9f77d1aa4'

        # 说明：主账号Token，登陆云通讯网站后，可在控制台-应用中看到开发者主账号AUTH TOKEN
        _accountToken = '5abf60d6b97448288e708c4aeaf3b842'

        # 请使用管理控制台首页的APPID或自己创建应用的APPID
        _appId = '8aaf070863f8fb0401641cb9f7df1aab'


    3.目前来说，其余代码不需要进行代码更改，另外就是，如何在服务器段完成具体的功能实现，调用该模块
        from info.libs.yuntongxun.sms import CCP
        # 6. 发送短信验证码
        result = CCP().send_template_sms(mobile, [sms_code_str, constants.SMS_CODE_REDIS_EXPIRES / 60], "1")
        if result != 0:
        # 代表发送不成功
        return jsonify(errno=RET.THIRDERR, errmsg="发送短信失败")
        """