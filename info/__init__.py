from logging.handlers import RotatingFileHandler

from flask import Flask
import logging

from flask import g
from flask import render_template
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.wtf import CSRFProtect
from flask.ext.wtf.csrf import generate_csrf
from redis import StrictRedis
from flask_session import Session
from config import config_dict


redis_store = None
db = SQLAlchemy()


def setup_log(config):
    """配置日志"""

    # 设置日志的记录等级
    logging.basicConfig(level=config_dict[config].LOG_LEVEL)  # 调试debug级
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)


def create_app(config):
    # 配置日志
    setup_log(config)
    app = Flask(__name__)
    app.config.from_object(config_dict[config])
    db.init_app(app)
    global redis_store
    redis_store = StrictRedis(host=config_dict[config].REDIS_HOST, port=config_dict[config].REDIS_PORT, decode_responses=True)
    # 这里注意为什么要在这里导入模块
    # 加载自定义过滤器
    from info.utils.common import do_news_status
    app.add_template_filter(do_news_status, "news_status")
    from info.utils.common import do_index_class
    app.add_template_filter(do_index_class, "index_class")
    from info.modules.index import index_blue
    app.register_blueprint(index_blue)
    from info.modules.passport import passport_blue
    app.register_blueprint(passport_blue)
    from info.modules.detail import news_blue
    app.register_blueprint(news_blue)
    from info.modules.profile import profile_blue
    app.register_blueprint(profile_blue)
    from info.modules.admin import admin_blue
    app.register_blueprint(admin_blue)
    CSRFProtect(app)
    Session(app)

    @app.after_request
    def after_request(response):
        csrf_token = generate_csrf()
        response.set_cookie("csrf_token", csrf_token)
        return response

    from info.utils.common import user_login_data

    # @app.errorhandler(404)
    # @user_login_data
    # def page_not_found(_):
    #     user = g.user
    #     data = {"user": user.to_dict() if user else None}
    #     return render_template('news/404.html', data=data)
    return app