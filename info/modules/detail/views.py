from flask import abort, jsonify
from flask import current_app
from flask import g
from flask import render_template
from flask import request

from info import constants, db
from info.models import News, Comment, CommentLike
from info.utils.common import user_login_data
from info.utils.response_code import RET
from . import news_blue


@news_blue.route('/comment_like', methods=["POST"])
@user_login_data
def set_comment_like():
    """
    评论点赞功能的实现，前台先判定当前评论点赞的状态，利用ajax项后台发出请求
    后台接受参数，然后根据实际情况进行点赞的操作或者取消操作
    :return:
    """

    if not g.user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    # 获取参数
    comment_id = request.json.get("comment_id")
    news_id = request.json.get("news_id")
    action = request.json.get("action")

    # 判断参数
    if not all([comment_id, news_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    if action not in ("add", "remove"):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 查询评论数据
    try:
        comment = Comment.query.get(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据失败")

    if not comment:
        return jsonify(errno=RET.NODATA, errmsg="评论数据不存在")

    if action == "add":
        comment_like = CommentLike.query.filter_by(comment_id=comment_id, user_id=g.user.id).first()
        if not comment_like:
            comment_like = CommentLike()
            comment_like.comment_id = comment_id
            comment_like.user_id = g.user.id
            db.session.add(comment_like)
            # 增加点赞条数
            comment.like_count += 1
    else:
        # 删除点赞数据
        comment_like = CommentLike.query.filter_by(comment_id=comment_id, user_id=g.user.id).first()
        if comment_like:
            db.session.delete(comment_like)
            # 减小点赞条数
            comment.like_count -= 1

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="操作失败")
    return jsonify(errno=RET.OK, errmsg="操作成功")


@news_blue.route("/news_comment", methods=['POST'])
@user_login_data
def news_comment():
    """
    完成用户评论的功能实现
    先从大的方面来讲分成三块
    1.后台，后台其实最简单理解，但也分为两部分，一是首页评论的展示，这个就是简单的模板的渲染，二是
    前台点击评论或者回复按钮时，后台接受参数，然后讲评论保存到数据库中
    2.前台模板部分其实也只是一个for循环而已，将后台返回的数据库中的数据进行渲染
    3.js部分，也是两部分，一就是主评论，即对新闻的评论，采用的是字符串拼接的方式，将最终展示的效果返回
    要着重理解为什么要采用这种方式
    :return:
    """
    user = g.user
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    news_id = request.json.get("news_id")
    content = request.json.get("comment")
    parent_id = request.json.get("parent_id")

    if not all([news_id, content]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询错误")

    if not news:
        return jsonify(errno=RET.NODATA, errmsg="未查询到新闻数据")
    comment = Comment()
    comment.news_id = news_id
    comment.content = content
    comment.user_id = user.id
    if parent_id:
        comment.parent_id = parent_id

    # 保存到数据库
    try:
        db.session.add(comment)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="保存评论数据失败")

    # 返回响应
    return jsonify(errno=RET.OK, errmsg="评论成功", data=comment.to_dict())


@news_blue.route("/news_collect", methods=['POST'])
@user_login_data
def news_collect():
    """
    完成收藏功能的后台实现
    首先后台要做的只有一件事，接受当前新闻是否被收藏的状态，然后进行切换
    前台传过来两个数据，news_id,
    :return:
    """
    user = g.user
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    # 1. 接受参数
    news_id = request.json.get("news_id")
    action = request.json.get("action")

    # 2. 判断参数
    if not all([news_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    if action not in ["collect", "cancel_collect"]:
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    try:
        news_id = int(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 3. 查询新闻，并判断新闻是否存在
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询错误")

    if not news:
        return jsonify(errno=RET.NODATA, errmsg="未查询到新闻数据")

    # 4. 收藏以及取消收藏
    if action == "cancel_collect":
        # 取消收藏
        if news in user.collection_news:
            user.collection_news.remove(news)
    else:
        # 收藏
        if news not in user.collection_news:
            # 添加到用户的新闻收藏列表
            user.collection_news.append(news)

    return jsonify(errno=RET.OK, errmsg="操作成功")


@news_blue.route("/<news_id>")
@user_login_data
def detail_news(news_id):
    """
    当前台点击某条新闻时，跳转页面访问相信的该函数，根据新闻id，查询对应内容，然后渲染模板
    因为在显示的时候，要进行判定，所以有必要
    :param news_id:
    :return:
    """
    # 查询登录状态
    user = g.user
    # 右侧的新闻排行的逻辑
    news_list = []
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    # 定义一个空的字典列表，里面装的就是字典
    news_rank_list = []
    # 遍历对象列表，将对象的字典添加到字典列表中
    for news in news_list:
        news_rank_list.append(news.to_basic_dict())

    # 查询新闻数据
    news = None
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
    if not news:
        # 报404错误，404错误统一显示页面后续再处理
        abort(404)

    # 是否是收藏　
    is_collected = False

    # if 用户已登录：
    #     判断用户是否收藏当前新闻，如果收藏：
    #         is_collected = True

    if user:
        # 判断用户是否收藏当前新闻，如果收藏：
        # collection_news 后面可以不用加all，因为sqlalchemy会在使用的时候去自动加载
        if news in user.collection_news:
            is_collected = True
    # 展示当前新闻的所有评论
    comments = []
    try:
        comments = Comment.query.filter(Comment.news_id == news_id).order_by(Comment.create_time.desc()).all()
    except Exception as e:
        current_app.logger.error(e)
    comment_list = []

    """显示当前评论的点赞状态"""
    comment_like_id = []
    if user:
    # comments = Comment.query.filter(Comment.news_id == news_id)
        try:
            comment_likes = CommentLike.query.filter(CommentLike.comment_id.in_([comment.id for comment in comments]),
                                             CommentLike.user_id==user.id).all()
            comment_like_id = [comment_like.comment_id for comment_like in comment_likes]
        except Exception as e:
            current_app.logger.error(e)

    for item in comments if comments else []:
        comment_dict = item.to_dict()
        comment_dict["is_like"] = False
        # 判断用户是否点赞该评论
        if g.user and item.id in comment_like_id:
            comment_dict["is_like"] = True
        comment_list.append(comment_dict)

    data = {
        "user":user.to_dict() if user else None,
        "news":news.to_dict(),
        "news_rank_list":news_rank_list,
        "is_collected":is_collected,
        "comments":comment_list
    }
    return render_template("news/detail.html", data=data)