from flask import current_app, jsonify
from flask import g
from flask import render_template
from flask import request

from info import constants
from info.models import Category, News
from info.utils.common import user_login_data
from info.utils.response_code import RET
from . import index_blue


@index_blue.route("/news_list")
def news_list():
    """
    这个路由函数用来显示默认页的新闻列表，显然是分页展示，当用户将下拉条拉到下方指定位置，
    则前台发送请求，后台返回下一页的内容
    这里面有不少的细节，返回新闻数据需要一定的时间，这个时间内用户重复拉动进度条是不会实现再次加载的，直到数据返回成功
    拿到数据之后，无非就是模板的渲染，但是因为新闻有分类，所以，首先得实现新闻分类的渲染
    :return:
    """
    # 1. 获取参数
    # 新闻的分类id
    cid = request.args.get("cid", "1")
    page = request.args.get("page", "1")
    per_page = request.args.get("per_page", "10")
    # 2. 校验参数
    try:
        page = int(page)
        cid = int(cid)
        per_page = int(per_page)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="参数")
    # 3. 查询数据
    # 这个条件的加入是为了标记后面的新闻的审核状态
    filters = [News.status == 0]
    if cid != 1:  # 查询的不是最新的数据
        # 需要添加条件
        filters.append(News.category_id == cid)
    try:
        paginate = News.query.filter(*filters).order_by\
            (News.create_time.desc()).paginate(page, per_page, False)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询错误")
        # 取到当前页的数据
    news_model_list = paginate.items  # 模型对象列表
    total_page = paginate.pages
    current_page = paginate.page
    # 将模型对象列表转换成字典列表
    news_dict_list = list()
    for news in news_model_list:
        news_dict_list.append(news.to_basic_dict())
    data = {
        "total_page": total_page,
        "current_page": current_page,
        "news_dict_list": news_dict_list
    }
    return jsonify(errno=RET.OK, errmsg="OK", data=data)


@index_blue.route("/")
@user_login_data
def index():
    user = g.user
    """默认页新闻排行的展示"""
    news_rank_list = []
    try:
        news_rank_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)
    news_dict_list = []
    for news in news_rank_list:
        news_dict_list.append(news.to_basic_dict())
    """实现对新闻分类的渲染"""
    categories = Category.query.all()
    category_list = []
    for category in categories:
        category_list.append(category.to_dict())

    data = {
        "user":user.to_dict() if user else None,
        "category_list":category_list,
        "news_rank_list":news_dict_list
    }
    return render_template('news/index.html', data=data)


# 加载title图标
@index_blue.route("/favicon.ico")
def favicon():
    # print("1")
    return current_app.send_static_file("news/favicon.ico")