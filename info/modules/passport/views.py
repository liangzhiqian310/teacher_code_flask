import random
import re

from datetime import datetime
from flask import abort, jsonify
from flask import current_app
from flask import make_response
from flask import request
from flask import session

from info import constants, db
from info import redis_store
from info.libs.yuntongxun.sms import CCP
from info.models import User
from info.utils.captcha import captcha
from info.utils.response_code import RET
from . import passport_blue


@passport_blue.route("/logout")
def logout():
    """
    登出操作首先，用户点击退出，前段发送ajax请求，然后后台将session中的用户信息删除，然后前台刷新页面，
    这时候因为用户信息没有，所以就实现了登出操作
    :return:
    """
    # pop是移除session中的数据(dict)
    # pop 会有一个返回值，如果要移除的key不存在，就返回None
    session.pop('user_id', None)
    session.pop('mobile', None)
    session.pop('nick_name', None)
    # 要清楚is_admin的值，如果不清除，先登录管理员，会保存到session，再登录普通用户，又能访问管理员页面
    session.pop('is_admin', None)

    return jsonify(errno=RET.OK, errmsg="退出成功")


@passport_blue.route("/login", methods=['POST'])
def login():
    """
    当用户点击登陆按钮，前台向后台发送ajaxpost请求，带上用户所输入的手机号码和密码，
    后台进行校验，先看是否有对应的用户存在，如果存在再校验密码是否正确，如果正确则将用户信息存在session中，同时刷新界面
    :return:
    """
    # 1. 获取参数
    # print(url_for("passport.log"))
    params_dict = request.json
    mobile = params_dict.get("mobile")
    password = params_dict.get("passport")
    # 2. 校验参数
    if not all([mobile, password]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")
    # 校验手机号是否正确
    if not re.match('1[35678]\\d{9}', mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手机号格式不正确")
    # 3. 判定用户是否存在，对比密码，匹配信息
    try:
        user = User.query.filter(User.mobile == mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询错误")
    if not user:
        return jsonify(errno=RET.NODATA, errmsg="用户不存在")
    if not user.check_password(password):
        return jsonify(errno=RET.PWDERR, errmsg="用户名或者密码错误")
    # 4. 设置cookie，保存用户登陆状态
    session["user_id"] = user.id
    session["mobile"] = user.mobile
    session["nick_name"] = user.nick_name

    # 设置当前用户最后一次登录的时间
    user.last_login = datetime.now()

    # 如果在视图函数中，对模型身上的属性有修改，那么需要commit到数据库保存
    # 但是其实可以不用自己去写 db.session.commit(),前提是对SQLAlchemy有过相关配置
    # try:
    #     db.session.commit()
    # except Exception as e:
    #     db.session.rollback()
    #     current_app.logger.error(e)
    # 5. 响应
    return jsonify(errno=RET.OK, errmsg="登录成功")


@passport_blue.route("/register", methods=['POST'])
def register():
    """
    当前端用户点击注册按钮时，前台向后台发送ajaxpost请求，后台接受三个参数，包括手机号码，手机验证码，密码
    后台先对手机号码和密码进行规则校验，然后在比对手机验证码是否正确，如果成功，则完成注册，
    初始化数据库表格模型，将用户信息存在数据库中，同时讲用户信息存在session中，再在index界面中进行判定，如果能从session中取到
    用户的信息则表示用户已经登陆，然后显示用户信息，所以注册完毕之后需要刷新一些界面
    :return:
    """
    """注册
            1.获取参数
            2.校验参数
            3.匹配手机验证码
            4.如果对比成功
            5.初始化User表格，赋值属性
            6.保存数据到数据库
            7.返回响应
            """
    # 1. 获取参数
    params_dict = request.json
    mobile = params_dict.get("mobile")
    smscode = params_dict.get("smscode")
    password = params_dict.get("password")

    # 2. 校验参数
    if not all([mobile, smscode, password]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数")

    # 校验手机号是否正确
    if not re.match('1[35678]\\d{9}', mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手机号格式不正确")

    # 3. 取到服务器保存的真实的短信验证码内容
    try:
        real_sms_code = redis_store.get("SMS_" + mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")

    if not real_sms_code:
        return jsonify(errno=RET.NODATA, errmsg="验证码已过期")
    # 4. 校验用户输入的短信验证码内容和真实验证码内容是否一致
    if real_sms_code != smscode:
        return jsonify(errno=RET.DATAERR, errmsg="验证码输入错误")

    # 5. 如果一致，初始化User，并赋值属性
    user = User()
    user.mobile = mobile
    user.nick_name = mobile
    user.last_login = datetime.now()
    user.password = password

    # 6. 添加到数据库
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="数据保存失败")

    # 往 session 中保存数据表示当前已经登录
    session["user_id"] = user.id
    session["mobile"] = user.mobile
    session["nick_name"] = user.nick_name

    # 7. 返回响应
    return jsonify(errno=RET.OK, errmsg="注册成功")


@passport_blue.route("/sms_code", methods=['POST'])
def send_sms_code():
    """
    接受前段发送的ajax请求，接受参数，包括手机号码，图片验证码，以及随机值，
    然后对手机号码进行一定的正则校验，然后从redis中取出保存的，根据随机值取出真实的验证码然后进行比对，如果成功
    然后生成一个随机的六位数字，然后借助第三方七牛云项手机发送短信
    :return:
    """
    """发送验证码短息的逻辑
            1.接受参数，手机号码，验证码，验证码随机编号
            2.校验参数
            3.在redis中根据编号取出正确的验证码
            4.如果比对不一致，返回验证码输入不正确
            5.如果比对正确，生成随机验证码数值
            6.发送短信验证码，记得保存到redis中
            7.告知结果，以便倒计时"""
    '{"mobile": "18811111111", "image_code": "AAAA", "image_code_id": "u23jksdhjfkjh2jh4jhdsj"}'
    # 1. 获取参数：手机号，图片验证码内容，图片验证码的编号 (随机值)
    # params_dict = json.loads(request.data)  request data 返回的是文本格式，为字符串，将格式转换为json格式
    params_dict = request.json  # flask特有方式
    """从请求中提取数据，那么必须明确的是数据是以什么形式发送过来的，不同的格式请求方式不同"""
    mobile = params_dict.get("mobile")
    image_code = params_dict.get("image_code")
    image_code_id = params_dict.get("image_code_id")
    # 2. 校验参数，判断参数是否存在且符合规则
    if not all([mobile, image_code, image_code_id]):
        # {"errno": "4100", "errmsg": "参数有误"}
        return jsonify(errno=RET.PARAMERR, errmsg="参数有误")
    # 校验手机号是否正确
    if not re.match('1[35678]\\d{9}', mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手机号格式不正确")
    # 3.从redis中取出对应的正确的验证码
    try:
        real_image_code = redis_store.get("ImageCodeId" + image_code_id)
    # 输出结果为byte
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")
    if not real_image_code:
        return jsonify(errno=RET.NODATA, errmsg="图片验证码已过期")
    # 4. 与用户的验证码内容进行对比，如果对比不一致，那么返回验证码输入错误
    # print(real_image_code.decode("utf-8"))
    # print(image_code)
    # if real_image_code.decode("utf-8").upper() != image_code.upper():
    if real_image_code.upper() != image_code.upper():
        return jsonify(errno=RET.DATAERR, errmsg="验证码输入错误")
    # 5. 如果一致，生成短信验证码的内容(随机数据)
    # 随机数字 ，保证数字长度为6位，不够在前面补上0
    sms_code_str = "%06d" % random.randint(0, 999999)
    current_app.logger.debug("短信验证码内容是：%s" % sms_code_str)
    # 6. 发送短信验证码
    result = CCP().send_template_sms(mobile, [sms_code_str, constants.SMS_CODE_REDIS_EXPIRES / 60], "1")
    if result != 0:
        # 代表发送不成功
        return jsonify(errno=RET.THIRDERR, errmsg="发送短信失败")
    # 保存验证码内容到redis
    try:
        # 设置过期时间
        redis_store.set("SMS_" + mobile, sms_code_str, constants.SMS_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据保存失败")
    # 7. 告知发送结果
    return jsonify(errno=RET.OK, errmsg="发送成功")


@passport_blue.route("/image_code")
def get_image_code():
    """
    接受前端的get请求，获取随机码，借助第三方模块生成图片验证码，然后存在redis中
    :return:
    """
    # 1.接受参数
    image_code_id = request.args.get('imageCodeId')
    # 2.校验，判断是否有值
    if not image_code_id:
        # 错误代码
        return abort(403)
    # 3.生成图片验证码
    name, text, image = captcha.captcha.generate_captcha()
    current_app.logger.debug("图片验证码内容是：%s" % text)
    # 保存图片验证码内容到redis
    try:
        redis_store.set("ImageCodeId" + image_code_id, text, constants.IMAGE_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        abort(500)
    # 5.返回验证码图片
    response = make_response(image)
    # 设置数据的类型，以便浏览器更加智能识别其是什么类型
    response.headers["Content-Type"] = "image/jpg"
    return response